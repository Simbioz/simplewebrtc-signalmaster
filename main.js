var express = require('express')
var sockets = require('signal-master/sockets')

var config = {
  "isDev": true,
  "server": {
    "port": process.env.PORT,
    "secure": false,
    "key": null,
    "cert": null,
    "password": null
  },
  "rooms": {
    "maxClients": 0
  },
  "stunservers": [],
  "turnservers": []
};

var app = express()

var server = app.listen(process.env.PORT)
sockets(server, config)