# SimpleWebRTC-SignalMaster

A SimpleWebRTC "signalmaster" server configured for hosting in IIS.

## Installation & Configuration

1. Install 'node'
2. Install the `iisnode` [module](https://github.com/tjanczuk/iisnode)
3. Create a site in IIS (as always)
4. Copy the server's files in the site's directory
5. Run `npm install -g windows-build-tools`
6. Run `npm install` to install the server's dependencies
7. Try it!

## Important Parts

- `web.config` tells IIS to host using the `iisnode` module
- `web.config` rewrites URLs to map to the server script file (`main.js`)
- `web.config` configures error passthrough (see comments)
- In `main.js`, we use `process.env.PORT` to use the port configured in IIS for the site
- We do not use the "secure" feature of the server as IIS takes care of this for us (it acts as a reverse proxy to the non-https signalmaster instance). This simplifies SSL certificates a great deal, among other things.